const express = require("express");

const app = express();

const router = express.Router();

router.get("/", (_, res) => {
    return res.status(200).json({ serverUp: true });
});

app.use("/", router);

app.listen(8000, () => console.log("started"));
